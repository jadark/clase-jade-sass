jQuery(document).ready(function($){
    var tpa_settings = {
      autoplay: false,
      showPlaylist: true,
      twitterText: "",
      allowShare: false,
      allowShuffle: true,
    }
    var tpa_controls = {
// Контейнеры
        wrapper: $('#tpa_wrapper'),
        audio: $('#tpa_audio'),
        timeRange: $('#tpa_timerange'),
        current: $('#tpa_current'),
        duration: $('#tpa_duration'),
        progress: $('#tpa_progress'),
        songName: $('#tpa_top'),
        volumeRange: $('.tpa_volumerange'),
        volumeRangeVal: $('.tpa_volumerange_value'),
        volumeVal: $('.tpa_volume_value'),
        topTitle: $('#tpa_top'),
        title: $('.tpa_title'),
        artist: $('.tpa_artist'),
        cover: $('#tpa_cover'),
        playlist: $('#tpa_playlist'),
        playlistItem: $('.tpa_playlist_item'),
        share: $('#tpa_social_wrapper'),
        shareIcon: $('.tpa_social_button'),
        loader: $('.tpa_loading'),
// Кнопки
        PlayPauseToggleButton: $('.tpa_playpause_toggle'),
        prevButton: $('.tpa_prev'),
        nextButton: $('.tpa_next'),
        repeatButton: $('.tpa_repeat'),
        volumeButton: $('#tpa_volume'),
        playlistButton: $('.tpa_playlist_toggle'),
        shareButton: $('.tpa_share'),
        shuffleButton: $('.tpa_shuffle'),
// Значения
        currentItem: $('.tpa_playlist_item').first(),
        minutes:'',
        seconds:'',
        currentVal:'',
        volume: '',
//Флаги
        firstMode: true,
        loadmetadata: false,
        firstFlag: false,
        lastFlag: false,
        repeatMode: false,
        shareMode: false,
        shuffleMode: false,
        
//Функции
//Play-Pause
        togglePlayback: function(){
            if(tpa_controls.firstMode){
                if(!tpa_controls.shuffleMode){
                    tpa_controls.currentItem.addClass('tpa_current_item');
                    tpa_controls.firstMode=false;
                    tpa_controls.loadInfo();
                } else {
                    tpa_controls.currentItem= $('.tpa_playlist_item:nth-child('+tpa_controls.shuffleTrack(1, tpa_controls.playlistItem.length)+')').addClass('tpa_current_item');
                    tpa_controls.firstMode=false;
                    tpa_controls.loadInfo();
                }
            }
            if(!tpa_controls.audio[0].paused){
                tpa_controls.PlayPauseToggleButton.removeClass('tpa_pause');
                tpa_controls.PlayPauseToggleButton.addClass('tpa_play');
                tpa_controls.audio[0].pause();
            } else {
                tpa_controls.PlayPauseToggleButton.removeClass('tpa_play');
                tpa_controls.PlayPauseToggleButton.addClass('tpa_pause');
                tpa_controls.audio[0].play();
            }
        },
//Обновления информации о треке
        loadInfo: function(){
// Анимация смены инфы
            tpa_controls.cover.stop(true,true).animate({
                'opacity':'0',
                transform: 'scale(1.75,1.75) rotate(15deg)',
            },500).animate({
                'opacity':'1',
                transform: 'scale(1,1) rotate(0deg)',
            },500);
            tpa_controls.songName.stop(true,true).animate({
                'opacity':'0',
            },500).animate({
                'opacity':'1',
            },500);
// Смена инфы с задержкой
            setTimeout(function(){
                tpa_controls.cover.css("background-image","url("+tpa_controls.currentItem.attr('data-cover')+")");
                tpa_controls.topTitle.attr('title',tpa_controls.currentItem.attr('data-artist')+'-'+tpa_controls.currentItem.attr('data-title'));
                tpa_controls.artist.text(tpa_controls.currentItem.attr('data-artist'));
                tpa_controls.title.text(tpa_controls.currentItem.attr('data-title'));
            }, 500);
            tpa_controls.audio[0].src=tpa_controls.currentItem.attr('data-url');
            tpa_controls.audio[0].load();
            tpa_controls.audio[0].volume=tpa_controls.volumeVal.text()/100;
            tpa_controls.loadmetadata=false;
        },
// Обновление продолжительности и текущего времени
        timeChange: function(timerange,element){
            tpa_controls.minutes=Math.floor(timerange / 60);
            tpa_controls.seconds=Math.floor(timerange % 60);
            if(isNaN(tpa_controls.minutes) || tpa_controls.minutes=='Infinity'){
                tpa_controls.minutes=0;
            }
            if(isNaN(tpa_controls.seconds) || tpa_controls.seconds=='Infinity'){
                tpa_controls.seconds=0;
            }
            if(tpa_controls.minutes<10){
                tpa_controls.minutes='0'+tpa_controls.minutes;
            }
            if(tpa_controls.seconds<10){
                tpa_controls.seconds='0'+tpa_controls.seconds;
            }
            element.text(tpa_controls.minutes+':'+tpa_controls.seconds);
        },
// Генерируем рендомное число для случайного воспроизведения
        shuffleTrack: function(min, max) {
            var rand = min - 0.5 + Math.random() * (max - min + 1)
            rand = Math.round(rand);
            return rand;
        },
        init: function(){
            if(tpa_controls.playlistItem.length==1){
                tpa_settings.showPlaylist=false;
                tpa_controls.playlistButton.hide();
                tpa_controls.playlist.hide();
                tpa_controls.nextButton.hide();
                tpa_controls.prevButton.hide();
                tpa_controls.shuffleButton.hide();
                tpa_controls.loadInfo();
                if($("a").is(".tpa_buy")){
                    $('#tpa_bottom').append('<div class="tpa_one_item_buy"></div>')
                }
                if($("a").is(".tpa_dwn")){
                    $('#tpa_bottom').append('<div class="tpa_one_item_dwn"></div>')
                }
            }
            if(tpa_settings.autoplay){
                tpa_controls.togglePlayback();
            }
            if(tpa_settings.showPlaylist){
                tpa_controls.playlist.show();
                tpa_controls.playlistButton.removeClass('tpa_playlist_close');
                tpa_controls.playlistButton.addClass('tpa_playlist_open');
            } else {
                tpa_controls.playlist.hide();
                tpa_controls.playlistButton.removeClass('tpa_playlist_open');
                tpa_controls.playlistButton.addClass('tpa_playlist_close');
            }
            if(!tpa_settings.allowShare){
                tpa_controls.shareButton.hide();
            }
            if(!tpa_settings.allowShuffle){
                tpa_controls.shuffleButton.hide();
            }
            tpa_controls.playlistItem.each(function(idx,value){
                $(this).find('span').css({
                    "-webkit-animation-delay": idx*50 + "ms",
                    "-moz-animation-delay": idx*50 + "ms",
                    "-ms-animation-delay": idx*50 + "ms",
                    "-o-animation-delay": idx*50 + "ms",
                    "animation-delay": idx*50 + "ms"
                });
            });
        },
// Анимация кнопок
        animationButton: function(element){
            element.stop(true,true).animate({
                transform: 'scale(0.75,0.75)',
            },200,"easeOutCirc").animate({
                transform: 'scale(1,1)',
            },600,"easeOutElastic");
        },
    };

/* Социальные кнопки */
    tpa_share={
        facebook: function(){
            url= "http://www.facebook.com/sharer.php?";   
            url+='u=' +encodeURIComponent(location.href);
            tpa_share.popup(url);
        },
        twitter: function(){
            url  = 'http://twitter.com/share?';
            url += 'text='      + encodeURIComponent(tpa_settings.twitterText);
            url += '&url='      + encodeURIComponent(location.href);
            tpa_share.popup(url);
        },
        googlePlus: function(){
            url= "https://plus.google.com/share?";   
            url+='url=' +encodeURIComponent(location.href);
            tpa_share.popup(url);
        },
        tumblr: function(){
            url= "http://tumblr.com/widgets/share/tool?";   
            url+='canonicalUrl=' +encodeURIComponent(location.href);
            tpa_share.popup(url);
        },
        popup: function(){
            window.open(url,'','toolbar=0,status=0,width=626,height=436');
        }
    };

/* Инициализируем пользовательские настройки, автостарт, плейлист, социальные кнопки */
    tpa_controls.init();

// скачка покупка когда один трек
    $('body').delegate('.tpa_one_item_buy', 'click', function(){
        href=$('.tpa_buy').attr('href');
        console.log(href)
        window.open(href, '_blank');
    });
    
    $('body').delegate('.tpa_one_item_dwn', 'click', function(){
        href=$('.tpa_dwn').attr('href');
        console.log(href)
        window.open(href, '_blank');
    });
    
    
    
// Воспроизвести/Пауза Кнопка
    tpa_controls.PlayPauseToggleButton.click(function(){
        tpa_controls.togglePlayback();
    });

// Предыдущий трек Кнопка
    tpa_controls.prevButton.click(function(){
        tpa_controls.animationButton($(this));
        if(tpa_controls.shuffleMode){
            tpa_controls.currentItem.removeClass('tpa_current_item');
            tpa_controls.currentItem=$('.tpa_playlist_item:nth-child('+tpa_controls.shuffleTrack(1, tpa_controls.playlistItem.length)+')').addClass('tpa_current_item');
            tpa_controls.loadInfo();
            tpa_controls.togglePlayback();
        } else {
            if(tpa_controls.currentItem.is('.tpa_playlist_item:first')) {
                tpa_controls.firstFlag=true;
            }
            if(!tpa_controls.firstFlag){
                tpa_controls.currentItem.removeClass('tpa_current_item');
                tpa_controls.currentItem=tpa_controls.currentItem.prev().addClass('tpa_current_item');;
                tpa_controls.loadInfo();
                tpa_controls.togglePlayback();
            } else {
                tpa_controls.firstFlag=false;
                if (tpa_controls.repeatMode){
                    tpa_controls.currentItem.removeClass('tpa_current_item');
                    tpa_controls.currentItem=tpa_controls.playlistItem.last().addClass('tpa_current_item');;
                    tpa_controls.loadInfo();
                    tpa_controls.togglePlayback();
                } else {
                    tpa_controls.audio[0].currentTime=0;
                    tpa_controls.PlayPauseToggleButton.removeClass('tpa_pause');
                    tpa_controls.PlayPauseToggleButton.addClass('tpa_play');
                    tpa_controls.audio[0].pause();
                }
            }
        }
    });


// Повтор плейлиста Кнопка
    tpa_controls.repeatButton.click(function(){
        if(!tpa_controls.repeatMode){
            tpa_controls.repeatMode=true;
        } else {
            tpa_controls.repeatMode=false;
        }
        tpa_controls.repeatButton.toggleClass('tpa_active_button');
    });


// Следующий трек Кнопка
    tpa_controls.nextButton.click(function(){
        tpa_controls.animationButton($(this));
        if(tpa_controls.shuffleMode){
            tpa_controls.currentItem.removeClass('tpa_current_item');
            tpa_controls.currentItem=$('.tpa_playlist_item:nth-child('+tpa_controls.shuffleTrack(1, tpa_controls.playlistItem.length)+')').addClass('tpa_current_item');
            tpa_controls.loadInfo();
            tpa_controls.togglePlayback();
        } else {
            if(tpa_controls.currentItem.is('.tpa_playlist_item:last')) {
                tpa_controls.lastFlag=true;
            }
            if(!tpa_controls.lastFlag){
                tpa_controls.currentItem.removeClass('tpa_current_item');
                tpa_controls.currentItem=tpa_controls.currentItem.next().addClass('tpa_current_item');;
                tpa_controls.loadInfo();
                tpa_controls.togglePlayback();
            } else {
                tpa_controls.lastFlag=false;
                if(tpa_controls.repeatMode){
                    tpa_controls.currentItem.removeClass('tpa_current_item');
                    tpa_controls.currentItem=tpa_controls.playlistItem.first().addClass('tpa_current_item');;
                    tpa_controls.loadInfo();
                    tpa_controls.togglePlayback();
                } else {
                    tpa_controls.audio[0].currentTime=0;
                    tpa_controls.PlayPauseToggleButton.removeClass('tpa_pause');
                    tpa_controls.PlayPauseToggleButton.addClass('tpa_play');
                    tpa_controls.audio[0].pause();
                }
            }
        }
    });
    
    
// Случайное воспроизведение
    
    tpa_controls.shuffleButton.click(function(){
        tpa_controls.animationButton($(this));
        tpa_controls.shuffleButton.toggleClass('tpa_active_button');
        if(!tpa_controls.shuffleMode){
            tpa_controls.shuffleMode=true;
        } else {
            tpa_controls.shuffleMode=false;
        }
    });
    
// Открытие/Закрытие оциальных кнопок
    tpa_controls.shareButton.click(function(){
        tpa_controls.animationButton($(this));
        tpa_controls.shareButton.toggleClass('tpa_active_button');
        if(!tpa_controls.shareMode){
            tpa_controls.shareMode=true;
            tpa_controls.share.addClass('tpa_active_share_wrapper');
        } else {
            tpa_controls.shareMode=false;
            tpa_controls.share.removeClass('tpa_active_share_wrapper');
        }
        
    });

    
// Открытие/Закрытие плейлиста Кнопка
    tpa_controls.playlistButton.click(function(){
        if(tpa_settings.showPlaylist){
            tpa_settings.showPlaylist=false;
            tpa_controls.playlist.slideUp(600,"easeInQuart");
            tpa_controls.playlistButton.removeClass('tpa_playlist_open');
            tpa_controls.playlistButton.addClass('tpa_playlist_close');
        } else {
            tpa_settings.showPlaylist=true;
            tpa_controls.playlist.slideDown(600,"easeOutQuart");
            tpa_controls.playlistButton.removeClass('tpa_playlist_close');
            tpa_controls.playlistButton.addClass('tpa_playlist_open');
        }
    });


// Включение/отключение звука Кнопка
    tpa_controls.volumeButton.click(function(){
        tpa_controls.animationButton($(this));
        if(!tpa_controls.audio[0].muted){
            tpa_controls.audio[0].muted=true;
            tpa_controls.audio[0].volume=0;
        } else {
            tpa_controls.audio[0].muted=false;
            tpa_controls.audio[0].volume=1;
        }
    });


// Перемотка аудио
    tpa_controls.timeRange.click(function(event){
        tpa_controls.currentVal = ((event.offsetX==undefined?event.layerX:event.offsetX)/tpa_controls.timeRange.width())*100;
        tpa_controls.audio[0].currentTime=(tpa_controls.audio[0].duration/100)*tpa_controls.currentVal;
        tpa_controls.progress.css('width',tpa_controls.currentVal+'%');
        tpa_controls.timeChange(tpa_controls.audio[0].currentTime,tpa_controls.current);
    });


// Изменение громкости
    tpa_controls.volumeRange.click(function(event){
        tpa_controls.volume = ((event.offsetX==undefined?event.layerX:event.offsetX)/tpa_controls.volumeRange.width())*100;
        if(tpa_controls.volume>95){
            tpa_controls.volume=100;
        } 
        else if(tpa_controls.volume<5){
            tpa_controls.volume=0;
        }
        tpa_controls.audio[0].volume=tpa_controls.volume/100;
        tpa_controls.volumeRangeVal.css('width',tpa_controls.volume+'%');
        tpa_controls.volumeVal.text((tpa_controls.volume).toFixed(0));
    });


// Плейлист
    tpa_controls.playlistItem.click(function(){
        tpa_controls.animationButton($(this).find('span'));
        if(tpa_controls.firstMode){
            tpa_controls.firstMode=false;
        }
        tpa_controls.currentItem.removeClass('tpa_current_item');
        tpa_controls.currentItem=$(this).addClass('tpa_current_item');
        tpa_controls.loadInfo();
        tpa_controls.togglePlayback();
    });


// События

// Событие по окончанию воспроизведения, запускаем следующий трек
    tpa_controls.audio[0].addEventListener('ended', function() {
        if(tpa_controls.shuffleMode){
            tpa_controls.currentItem.removeClass('tpa_current_item');
            tpa_controls.currentItem=$('.tpa_playlist_item:nth-child('+tpa_controls.shuffleTrack(1, tpa_controls.playlistItem.length)+')').addClass('tpa_current_item');
            tpa_controls.loadInfo();
            tpa_controls.togglePlayback();
        } else {
            if (tpa_controls.currentItem.is('.tpa_playlist_item:last')) {
                tpa_controls.lastFlag=true;
            }
            if(!tpa_controls.lastFlag){
                tpa_controls.currentItem.removeClass('tpa_current_item');
                tpa_controls.currentItem=tpa_controls.currentItem.next().addClass('tpa_current_item');;
                tpa_controls.loadInfo();
                tpa_controls.togglePlayback();
            } else {
                tpa_controls.lastFlag=false;
                if (tpa_controls.repeatMode){
                    tpa_controls.currentItem.removeClass('tpa_current_item');
                    tpa_controls.currentItem=tpa_controls.playlistItem.first().addClass('tpa_current_item');;
                    tpa_controls.loadInfo();
                    tpa_controls.togglePlayback();
                } else {
                    tpa_controls.audio[0].currentTime=0;
                    tpa_controls.PlayPauseToggleButton.removeClass('tpa_pause');
                    tpa_controls.PlayPauseToggleButton.addClass('tpa_play');
                    tpa_controls.audio[0].pause();
                }
            }
        }
    });

// включаем загрузчик
    tpa_controls.audio[0].addEventListener('loadstart', function() {
        tpa_controls.loader.show();
    });
    
    tpa_controls.audio[0].addEventListener('waiting', function() {
        tpa_controls.loader.show();
    });
// включаем загрузчик
    tpa_controls.audio[0].addEventListener('canplay', function() {
        tpa_controls.loader.hide();
    });
    
    tpa_controls.audio[0].addEventListener('playing', function() {
        tpa_controls.loader.hide();
    });

// Загрузилась метадата вычесляем продолжительность
    tpa_controls.audio[0].addEventListener('loadedmetadata', function() {
        tpa_controls.timeChange(tpa_controls.audio[0].duration,tpa_controls.duration);
        tpa_controls.loadmetadata=true;
    });

// Если изменилась продолжительность вычесляем продолжительность
    tpa_controls.audio[0].addEventListener('durationchange', function() {
        tpa_controls.timeChange(tpa_controls.audio[0].duration,tpa_controls.duration);
        tpa_controls.loadmetadata=true;
    });


// Изменения текущего времени
    tpa_controls.audio[0].addEventListener('timeupdate', function() {
        tpa_controls.timeChange(tpa_controls.audio[0].currentTime,tpa_controls.current);
        tpa_controls.progress.css('width',(tpa_controls.audio[0].currentTime/tpa_controls.audio[0].duration*100).toFixed(2)+'%');
        
    });

// Изменения громкости
    tpa_controls.audio[0].addEventListener('volumechange', function() {
        tpa_controls.volume=tpa_controls.audio[0].volume;
        if(tpa_controls.volume>0.75){
            tpa_controls.volumeButton.removeClass().addClass('tpa_volume_up');
            tpa_controls.audio[0].muted=false;
        }
        else if(tpa_controls.volume<0.5 && tpa_controls.volume>0.25){
            tpa_controls.volumeButton.removeClass().addClass('tpa_volume_low');
            tpa_controls.audio[0].muted=false;
        }
        else if(tpa_controls.volume<0.25 && tpa_controls.volume!=0){
            tpa_controls.volumeButton.removeClass().addClass('tpa_volume_muted');
            tpa_controls.audio[0].muted=false;
        }
        else if(tpa_controls.volume==0 || tpa_controls.audio[0].muted){
            tpa_controls.audio[0].muted=true;
            tpa_controls.volumeButton.removeClass().addClass('tpa_volume_muted').addClass('tpa_active_button');
        }
        tpa_controls.volumeRangeVal.css('width',tpa_controls.volume*100+'%');
        tpa_controls.volumeVal.text((tpa_controls.volume*100).toFixed(0));
    });

// резиновая верстка
    if(tpa_controls.wrapper.width()<600){
        $('#tpa_player').css('height', '320px');
        $('#tpa_left').css({
            'width':'100%',
            'display':'block',
        });
        $('#tpa_cover').css({
            'width':'100%',
            'height':'160px',
            'display':'block',
        });
        $('#tpa_right').css({
            'width':'100%',
            'display':'block',
        });
        
    } 
    if(tpa_controls.wrapper.width()<500){
        $('#tpa_middle').css({
            'height':'50px',
            'text-align':'center',
            'border-spacing':'0px;',
        });
        $('.tpa_playpause_toggle').css({
            'display':'inline-block',
            'margin':'0 10px',
        });
        $('.tpa_prev').css({
            'display':'inline-block',
            'margin':'0 10px',
        });
        $('.tpa_repeat').css({
            'display':'inline-block',
            'margin':'0 10px',
        });
        $('.tpa_next').css({
            'display':'inline-block',
            'margin':'0 10px',
        });
        $('.tpa_shuffle').css({
            'display':'inline-block',
            'margin':'0 10px',
        });
        /*$('.tpa_share').css({
            'display':'inline-block',
            'margin':'0 10px',
        });*/
        $('.tpa_playlist_toggle').css({
            'display':'inline-block',
            'margin':'0 10px',
            'left':'10px',
        });
        $('.tpa_volume').css({
            'display':'inline-block',
            'margin':'0 10px',
        });
        $('#tpa_timerange').css({
            'position':'absolute',
            'bottom':'0',
            'left':'20px',
            'right':'20px',
            'height':'20px',
            'cursor':'pointer',
            'display':'block',
            'overflow':'hidden',
        });
        $('#tpa_bottom').css({
            'width':'100%',
            'height':'60px',
        });
    } 

    $('.tpa_dwn').click(function(){
        window.open($(this).attr('href'), '_blank');
        return false;
    });
    
    $('.tpa_buy').click(function(){
        window.open($(this).attr('href'), '_blank');
        return false;
    });
    
});
